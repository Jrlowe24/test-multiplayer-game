let Player = require('./Player');

let PLAYER_LIST = {};
let methods = {};

methods.createPlayer = function (id) {
  PLAYER_LIST[id] = new Player(id);
  console.log('Creating Player: ', PLAYER_LIST[id]);
};

methods.removePlayer = function (id) {
  console.log('Removing Player: ', PLAYER_LIST[id]);
  delete PLAYER_LIST[id];
};

methods.getPlayer = function (id) {
  return PLAYER_LIST[id];
};

methods.getPlayerList = function () {
  return PLAYER_LIST;
};

methods.checkInput = function (id, data) {

  PLAYER_LIST[id].applyInput(data);
  // console.log(data);
  PLAYER_LIST[id].last_processed_input = data.input_sequence_number;
};


module.exports = methods;

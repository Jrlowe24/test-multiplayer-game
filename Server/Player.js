var Player = function(id) {
  var self = {
    x: 300,
    y: 300,
    id:id,
    number: "" + Math.floor(10 * Math.random()),
    // key_presses: {
    //   w: false,
    //   s: false,
    //   a: false,
    //   d: false
    // },
    speed: 150,
    last_processed_input: null
  };

  self.applyInput = function (input) {

    let speed = this.speed;
    if (input.press_time_x !== 0 && input.press_time_y !== 0) {
      speed = speed / 1.41421;
    }
    self.x += input.press_time_x * speed;
    self.y += input.press_time_y * speed;
    // self.y += input.press_time * self.speed;
  };
  return self;
};

module.exports = Player;

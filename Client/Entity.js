Entity = function(id) {

  this.x = 300;
  this.y = 300;
  this.number = 0;
  this.id = id;
  this.position_buffer = [];
  this.speed = 20;

  this.render = function (c) {

    c.beginPath();
    c.arc(this.x, this.y, 40, 0, 2 * Math.PI, false);
    c.fillStyle = 'purple';
    c.fill();
    c.lineWidth = 5;
    c.strokeStyle = '#003300';
    c.stroke();

  };

  this.interpolate = function () {
    let now = +new Date();
    let render_timestamp = now - (1000.0 / 20);

    // Find the two authoritative positions surrounding the rendering timestamp.
    // Drop older positions.
    while (this.position_buffer.length >= 2 && this.position_buffer[1][0] <= render_timestamp) {
      this.position_buffer.shift();
    }

    // Interpolate between the two surrounding authoritative positions.
    if (this.position_buffer.length >= 2 && this.position_buffer[0][0] <= render_timestamp && render_timestamp <= this.position_buffer[1][0]) {
      let x0 = this.position_buffer[0][1];
      let x1 = this.position_buffer[1][1];
      let y0 = this.position_buffer[0][2];
      let y1 = this.position_buffer[1][2];
      let t0 = this.position_buffer[0][0];
      let t1 = this.position_buffer[1][0];

      this.x = x0 + (x1 - x0) * (render_timestamp - t0) / (t1 - t0);
      this.y = y0 + (y1 - y0) * (render_timestamp - t0) / (t1 - t0);
    }
  };

  this.applyInput = function (input) {
    this.x += input.press_time_x * this.speed;
    this.y += input.press_time_y * this.speed;
  };
};



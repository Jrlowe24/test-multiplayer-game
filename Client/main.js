const socket = io();

var SCREEN_WIDTH = window.innerWidth,
	SCREEN_HEIGHT = window.innerHeight,
	HALF_WIDTH = window.innerWidth / 2,
	HALF_HEIGHT = window.innerHeight / 2;

let latency = 0;
let startTime;

let OTHER_PLAYERS = [];
let player = null;

var canvas = document.createElement( 'canvas' ),
  context = canvas.getContext( '2d' );

window.addEventListener("load", init);

setInterval(function() {
	startTime = Date.now();
	socket.emit('lagPing');
}, 2000);

socket.on('lagPong', function() {
	latency = Date.now() - startTime;
});

// server sends 20 positional updates per second to update all entities
socket.on('newPositions', function(pack) {
	for (let i = 0; i < pack.length; i++) {
		let state = pack[i];
			if (pack[i].id === player.id) {
				player.x = pack[i].x;
				player.y = pack[i].y;

				let j = 0;
				while (j < player.pending_inputs.length) {
					var input = player.pending_inputs[j];
					if (input.input_sequence_number <= state.last_processed_input) {
						// Already processed. Its effect is already taken into account into the world update
						// we just got, so we can drop it.
						player.pending_inputs.splice(j, 1);
					} else {
						// Not processed by the server yet. Re-apply it.
						player.applyInput(input);
						j++;
					}
				}

			} else {
				let timestamp = +new Date();
				OTHER_PLAYERS[pack[i].id].position_buffer.push([timestamp, state.x, state.y]);
			}
		}
});

// when this client connects, the server tells us to create a player
socket.on('createPlayer', function(id) {
	player = new Player(id);
	console.log(player);
});

// Called everytime a new player joins the server, this keeps the player list synced with all clients
socket.on('updatePlayers', function(player_list) {
	// check if we need to remove any
	OTHER_PLAYERS = OTHER_PLAYERS.filter(i => player_list.some(j => j.id === i.id));
	for (let i in player_list) {
		let newPlayer = player_list[i];
		let exists = OTHER_PLAYERS.some(el => el.id === newPlayer.id) || newPlayer.id === player.id;
		if (!exists) {
			var entity = new Entity(newPlayer.id);
			entity.x = newPlayer.x;
			entity.y = newPlayer.y;
			entity.number = newPlayer.number;
			OTHER_PLAYERS[entity.id] = entity;
			console.log('New player Created: ', entity);
		}
	}
});

function init() {
	// CANVAS SET UP
	document.body.appendChild(canvas);
	canvas.width = SCREEN_WIDTH;
	canvas.height = SCREEN_HEIGHT;

	window.addEventListener('resize', resizeGame);
	window.addEventListener('orientationchange', resizeGame);

	gameLoop();
}

function gameLoop() {
  requestAnimationFrame(gameLoop);

	update();
  draw();
}

function update() {

	let now_ts = +new Date();
	let last_ts = this.last_ts || now_ts;
	let dt_sec = (now_ts - last_ts) / 1000.0;
	this.last_ts = now_ts;
	if (player) {
		let input = player.processInputs(dt_sec);
		if (input) {
			socket.emit('input', input);
		}
	}
	for (let i in OTHER_PLAYERS) {
		OTHER_PLAYERS[i].interpolate();
	}
}

function draw() {
	var c = context;
	c.fillStyle = "yellow";
	c.fillRect(0, 0, canvas.width, canvas.height);

	for (var i in OTHER_PLAYERS) {
		var otherPlayer = OTHER_PLAYERS[i];
		otherPlayer.render(c);
	}
	if (player !== null) {
		player.render(c);
	}
	c.font = "10px Arial";
	c.fillText("Ping: " + latency + 'ms', 10, 15);
}

function resizeGame(event) {
  var newWidth = window.innerWidth;
	var newHeight = window.innerHeight;

	if((SCREEN_WIDTH== newWidth) && (SCREEN_HEIGHT==newHeight)) return;

	SCREEN_WIDTH = canvas.width = newWidth;
	SCREEN_HEIGHT = canvas.height = newHeight;
}

var keyHandler = function(e) {
	e = e || window.event;
	if (e.key === 'w') {
		player.keyPresses.up = (e.type === "keydown");
	} else if (e.key === 's') {
		player.keyPresses.down = (e.type === "keydown");
	} else if (e.key === 'a') {
		player.keyPresses.left = (e.type === "keydown");
	} else if (e.key === 'd') {
		player.keyPresses.right = (e.type === "keydown");
	}
};
document.body.onkeydown = keyHandler;
document.body.onkeyup = keyHandler;

Player = function (id) {

  this.x = 300;
  this.y = 300;
  this.number = 0;
  this.id = id;
  this.input_sequence_number = 0;
  this.pending_inputs = [];
  this.speed = 150;
  this.keyPresses = {
    up: false,
    down: false,
    left: false,
    right: false
  };

  this.render = function (c) {

    c.beginPath();
    c.arc(this.x, this.y, 40, 0, 2 * Math.PI, false);
    c.fillStyle = 'green';
    c.fill();
    c.lineWidth = 5;
    c.strokeStyle = '#003300';
    c.stroke();
  };

  this.processInputs = function (dt_sec) {
    let input = {};
    input.press_time_x = 0;
    input.press_time_y = 0;

    if (this.keyPresses.left) {
      input.press_time_x = -dt_sec;
    } else if (this.keyPresses.right) {
      input.press_time_x = dt_sec;
    }
    if (this.keyPresses.up) {
      input.press_time_y = -dt_sec;
    } else if (this.keyPresses.down) {
      input.press_time_y = dt_sec;
    }

    if (input.press_time_y === 0 && input.press_time_x === 0) {
      return;
    }

    input.input_sequence_number = this.input_sequence_number++;
    input.entity_id = this.id;
    this.applyInput(input);
    this.pending_inputs.push(input);

    return input;
  };

  this.applyInput = function (input) {
    let speed = this.speed;
    if (input.press_time_x !== 0 && input.press_time_y !== 0) {
      speed = speed / 1.41421;
    }
    this.x += input.press_time_x * speed;
    this.y += input.press_time_y * speed;
  };
};

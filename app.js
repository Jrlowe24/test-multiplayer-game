const express = require('express');
const http = require('http');
const path = require('path');
const socketIO = require('socket.io');
const app = express();
const server = http.Server(app);
const io = socketIO(server);
let Game = require('./Server/Game');
// let Player = require('./Server/Player');
app.set('port', 5000);

app.use('/static', express.static(__dirname + '/static'));// Routing
app.get('/', function(request, response) {
  response.sendFile(path.join(__dirname, '/Client/index.html'));
});// Starts the server.
app.use('/Client', express.static(__dirname + '/Client'));
server.listen(5000, function() {
  console.log('Starting server on port 5000');
});

let SOCKET_LIST = {}; // List of all connected Clients

io.sockets.on('connection', function(socket) {
  console.log('Socket Connection');
  socket.id = Math.random();

  SOCKET_LIST[socket.id] = socket;
  Game.createPlayer(socket.id);

  socket.emit('createPlayer', socket.id);
  updateClientList(); // make sure all clients add the new player

  // console.log('Player List', Game.getPlayerList());

  socket.on('disconnect', function() {
    console.log('Disconnect');
    delete SOCKET_LIST[socket.id];
    Game.removePlayer(socket.id);
    updateClientList(); // clients will remove players that left
  });

  // socket.on('keyPress', function(data) {
  //   Game.checkInput(socket.id, data);
  // });

  socket.on('input', function(data) {
    Game.checkInput(socket.id, data);
  });

  socket.on('lagPing', function() {
    // console.log('ping received');
    socket.emit('lagPong');
  });

  // Use this to simulate network latency if you're running on local host
  // let ping = 150 // milliseconds
  // (function() {
  //   var oldEmit = socket.emit;
  //   socket.emit = function() {
  //     var args = Array.from(arguments);
  //     setTimeout(() => {
  //       oldEmit.apply(this, args);
  //     }, ping);
  //   };
  // })();

});

setInterval(function() {
  const pack = [];
  for (var i in Game.getPlayerList()) {
    var player = Game.getPlayerList()[i];
    // player.updatePosition();
    pack.push({
      x: player.x,
      y: player.y,
      number: player.number,
      id: player.id,
      last_processed_input: player.last_processed_input
    })
  }
  for (var i in SOCKET_LIST) {
    const socket = SOCKET_LIST[i];
    socket.emit('newPositions', pack);
  }

}, 1000/20);

updateClientList = function() {
  for (var i in SOCKET_LIST) {
    const socket = SOCKET_LIST[i];
    socket.emit('updatePlayers', Game.getPlayerList());
  }
};

